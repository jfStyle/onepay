Cmpay_v20190924(测试版本)
===============

### 项目介绍
***

- `onepay/Cmpay聚合支付`聚合支付使用ThinkPHP框架开发，已接入微信、支付宝等主流支付渠道；

- 目前已经接入支付渠道：微信(扫码支付)、支付宝(电脑网站支付、手机网站支付)；

- 本项目主要用于内部项目对接，并未考虑到一些场景需求，大家可以根据实际情况做下修改；



> 运行环境要求PHP > 5.6以上(推荐7.0.*)。

> 友情提示：本系统只作为研究和内部项目技术对接使用，如通过其或者其他延伸从事非法定商业资金结算以及盈利目的的，对此，概不负责！！！


使用系统前请：

>详细开发文档参考 [ThinkPHP5完全开发手册](http://www.kancloud.cn/manual/thinkphp5)

### 项目结构

```
project                             应用部署目录
│
├─application                       应用目录
│  ├─admin                          后台模块目录
│  ├─api                            API模块目录
│  ├─common                         公共模块目录
│  ├─index                          前端模块目录
│  ├─command.php                    命令行工具配置文件
│  ├─common.php                     应用公共（函数）文件
│  ├─config.php                     应用（公共）配置文件
│  ├─database.php                   数据库配置文件
│  ├─tags.php                       应用行为扩展定义文件
│  ├─route.php                      路由配置文件
│  └─...
├─data                              数据存储目录
│  ├─cret                           证书文件
│  ├─crond                          Cron定时文件
│  ├─extend                         拓展类库
│  ├─runtime                        系统运行runtime目录
│  ├─supervisord                    supervisord配置目录
│  └─...
├─public                            Web 部署目录（对外访问目录）
│  ├─static                         静态资源存放目录(css,js,image)
│  ├─upload                         系统文件上传存放目录
│  ├─index.php                      入口文件
│  ├─.htaccess                      用于 apache 的重写
│  └─...
├─build.php                         自动生成定义文件（参考）
├─composer.json                     composer 定义文件
├─LICENSE.txt                       授权说明文件
├─README.md                         README 文件
└─think                             命令行入口文件
```

## **在线演示**

暂无 


## **运营截图**


![输入图片说明](https://t2.wodetu.cn/2018/11/07/89b6b3846f0d6bedfe6a56e18218c7d0.png "运营平台")

![输入图片说明](https://t3.wodetu.cn/2018/11/07/d5d6a3563b9f926cfaa751814a61f0ad.png "运营平台")

![输入图片说明](https://t4.wodetu.cn/2018/11/07/bd094bcaa6aed7d6054e706dfa2155b7.png "运营平台")

![输入图片说明](https://t3.wodetu.cn/2018/11/07/945e8aca35925fc2210831fcb0b7eca2.png "运营平台")

![输入图片说明](https://t2.wodetu.cn/2018/11/07/19ebbd6a728bae38e0cbb776b59a252b.png "运营平台")

### 版本更新

版本 |日期 |描述
---------| -----------| ------
v2.0.1  | 2019-09-24 | 从2.0开始继续更新和完善，目前已经增加收银台(支付宝因没有账号未做测试和添加，修改修改就行)，后台增加ws服务用于收银台回调和刷新状态，微信授权获取openid。其他功能未完待续...
V1.0.3 | 2018-12-22 | 更改支付接入方式，修复部分BUG
V1.0.2 | 2018-12-15 | 更新SDK接入，修复部分BUG
V1.0.1 | 2018-12-10 | 更新部分，修复BUG
V1.0.0 |2018-11-12 |更换前端页面，完善部分功能，商户端开发进行中...
V1.0.0 |2018-11-06 |修复部分Bug,完善后台权限管理，数据统计，其他
V1.0.0 |2018-10-23 |最初版本

接下来的开源版本开发计划：
```html
+ 持续完善V2.0.0版本；
+ 增加渠道对接，API接口完善；
+ 完善支付核心；
+ 持续增加其他附属功能；
+ 构思...

```
其他想法，大家请提交issues。

## **问题反馈**

在使用中有任何问题，请使用以下联系方式联系我们


## **Pay对应PHP开发包**

https://github.com/iredcap/pay-sdk-php


## **支付包sdk单独使用示例**

https://github.com/iredcap/pay-sdk-php-demo


## **特别鸣谢**

感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

LayuiAdmin：https://www.layui.com/admin/ (商用请授权)

OneBase： https://www.onebase.org

原作者仓库：

Github: https://github.com/iredcap/pay

Gitee: https://gitee.com/iredcap/cmpay

## **版权信息**

`onepay/Cmpay聚合支付`遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017-2021 by zaite、iredcap)

All rights reserved。

